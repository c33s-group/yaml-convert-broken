# Changelog

All notable changes to this project will be documented in this file, in reverse chronological order by release.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [3.0.0] - 2019-07-12
### Added
- `lint` command to validate one ore more yaml files

### Changed
- reintroduced multiple commands to allow to use `lint` and `convert`



## [2.0.0] - 2019-02-19
### Added
- support for composer.yaml
- version by git tag
- phar compression

### Changed
- box.json -> box.json.dist
- updated dependencies
- default output file is now composer.yaml

### Removed
- convert command, the app is a single command app now (convert is default)



## [1.0.0] - 2013-07-10
### Added
- initial version from https://github.com/igorw/composer-yaml
