# yaml-convert

![Maintenance](https://img.shields.io/maintenance/yes/2021.svg)

Download [yaml-convert.phar][link_download_latest] (latest main)

this project is a fork of https://github.com/igorw/composer-yaml which looks abandoned. the purpose is still the same,
[`yaml-convert.phar`][link_download_latest] allows you to convert a `composer.yaml` file into `composer.json` format. 

Stable download url for download from gitlab package repository via public api:
```
https://gitlab.com/api/v4/projects/10924742/packages/generic/yaml-convert/<VERSION>/yaml-convert.phar
```

download it like:
```
wget https://gitlab.com/api/v4/projects/10924742/packages/generic/yaml-convert/3.0.2/yaml-convert.phar
```


see `CHANGELOG.md` for the detailed differences to the original project. the main differences are:

- support for `composer.yml` and `composer.yaml`
- example scripts to use
- downloadable phar file
- added `lint` command


**Warning: If you already have a composer.json file, it will overwrite it.**

## Usage

To convert from yaml to json, run:

    $ php yaml-convert.phar convert

To convert from json to yaml, run:

    $ php yaml-convert.phar convert composer.json composer.yaml

To lint a yaml file:

    $ php yaml-convert.phar lint myfile.yaml

[link_download_latest]: https://gitlab.com/c33s-group/yaml-convert/-/jobs/artifacts/main/raw/yaml-convert.phar?job=build
